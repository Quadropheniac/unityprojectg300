﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class PlayerController : MonoBehaviour {
    private Rigidbody _rb;
    public int Speed;
    public Text Score_Txt;
    private int Count;


	// Use this for initialization
	void Start () {
        _rb = GetComponent<Rigidbody>(); 	
	}

    private void FixedUpdate()
    {
        float moveVertical = Input.GetAxis("Vertical");
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);

        _rb.AddForce(movement * Speed); 
       

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up")) 
            {
                other.gameObject.SetActive(false);
            Count += 1;

            Score_Txt.text = "Count:" +Count.ToString();
            }
    }
}
